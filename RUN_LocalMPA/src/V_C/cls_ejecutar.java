/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package V_C;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author gilsonq
 */
public class cls_ejecutar {
    
    public String resultadoEjecutar(Process is) {
        String imprimirLinea="";
        BufferedReader inStream = null;
        try {
            inStream = new BufferedReader(
                    new InputStreamReader(is.getInputStream()));
            String capturar = inStream.readLine();
            while(capturar != null){
                imprimirLinea += capturar +"\n";
                capturar = inStream.readLine();
            }
        } catch (IOException e) {
            System.err.println("Error al capturar linea");
            e.printStackTrace();
        }
        return imprimirLinea;
    }
    public Process ejecutar(String comando) {
        Process funcion = null;      
        try {
            String [] comandos = {"bash", "-c",comando};
            funcion = Runtime.getRuntime().exec(comandos);
        } catch (IOException e) {
            System.err.println("Error al ejecutar");
            e.printStackTrace();
        }

        return funcion;        
    }
}
